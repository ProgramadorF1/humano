
package humano;

import java.util.Scanner;


public class Humano {
    
    private String Nombre;
    private String Apellido;
    private int Edad;
    private double Peso;
    private double Altura;
    private double IMC;

    
    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public int getEdad() {
        return Edad;
    }

    public void setEdad(int Edad) {
        this.Edad = Edad;
    }

    public double getPeso() {
        return Peso;
    }

    public void setPeso(double Peso) {
        this.Peso = Peso;
    }

    public double getAltura() {
        return Altura;
    }

    public void setAltura(double Altura) {
        this.Altura = Altura;
    }

    public double getIMC() {
        return IMC;
    }

    public void setIMC(double IMC) {
        this.IMC = IMC;
    }
    
    public void CalcularIMC(double a, double p){
        double r = 0;
        a = a/100;
        r = a/p*p;
        this.IMC = r;
    }
    
    public String conceptoIMC (){
        if(this.IMC<18){
            return"Bajo de peso";
        }else if(this.IMC<24){
            return"Normal de peso";
        }else if(this.IMC<29){
            return"Gordo";
        }else{
            return"Obeso";
        }
    }
    
    public Humano(String N, String A){
        this.Nombre = N;
        this.Apellido = A;
        this.Edad = 0;
        this.Altura = 0;
        this.Peso = 0;
        this.IMC = 0;
    }
        
    public static void main(String[] args) {
        Humano humano1 = new Humano("Pedro","Morales");
        Scanner leer = new Scanner (System.in);
        double a = 0,p = 0;                
        
        System.out.println(humano1.getNombre()+" "+ humano1.getApellido());
        
        humano1.setEdad(15);
        System.out.println("la edad de "+ humano1.getNombre()+" es "+ humano1.getEdad());
        
        
        
        /*System.out.println("Ingresa altura");
        a = leer.nextDouble();
       // humano1.setAltura(a); 
        System.out.println("la altura de "+ humano1.getNombre()+" es "+ humano1.getAltura());*/
        
        
        
        /*System.out.println("Ingresa peso");
        p = leer.nextDouble();
        //humano1.setPeso(p);
        System.out.println("El peso de "+ humano1.getNombre()+" es "+ humano1.getPeso()); */       

        humano1.CalcularIMC(180 , 50 );
        System.out.println("El Indice de masa corporal es "+humano1.conceptoIMC());
        
        System.out.println(humano1.getIMC());
    }
}
